package com.esi_estech.project.screensharing;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * Created by hchen on 2/2/2016.
 */
public class Fragment1 extends Fragment {
    // Defines a tag for identifying log entries
    private static final String TAG = Fragment1.class.getSimpleName();

    private View mView;
    private TextView mTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment1, container, false);

        mTextView = (TextView)mView.findViewById(R.id.text1);

        return mView;
    }
}
