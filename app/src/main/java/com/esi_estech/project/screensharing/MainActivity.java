package com.esi_estech.project.screensharing;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    // Defines a tag for identifying log entries
    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int FRAGMENT_ID_1 = 1;
    private static final int FRAGMENT_ID_2 = 2;

    private Button mFragment1Btn;
    private Button mFragment2Btn;
    private Button mDialogBtn;
    private Button mDialogFragmentBtn;

    private Fragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFragment1Btn = (Button)findViewById(R.id.btn1);
        mFragment2Btn = (Button)findViewById(R.id.btn2);
        mDialogBtn = (Button)findViewById(R.id.btn3);
        mDialogFragmentBtn = (Button)findViewById(R.id.btn4);

        setupContent();

        setFragment(FRAGMENT_ID_1);
    }

    private void setupContent() {
        mFragment1Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment(FRAGMENT_ID_1);
            }
        });

        mFragment2Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment(FRAGMENT_ID_2);
            }
        });

        mDialogBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Share");
                ((Fragment2)mFragment).shareScreen();
            }
        });

        mDialogFragmentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Stop");
                ((Fragment2)mFragment).stopScreenSharing();
            }
        });
    }

    private void setFragment(int id) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if(id == FRAGMENT_ID_1)
            mFragment = new Fragment1();
        else
            mFragment = new Fragment2();

        fragmentTransaction.replace(R.id.fragment_container, mFragment);
        fragmentTransaction.commit();
    }

}
